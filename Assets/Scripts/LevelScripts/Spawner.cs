﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
	{

	private GameObject playerOne;

	private GameObject uiManager;
	private GameObject winMenu;
	public AudioSource bgm;

	// Color of the gizmo
	public Color gizmoColor = Color.red;
	
	//-----------------------------------
	// All the Enums
	//-----------------------------------
	// Spawn types
	public enum SpawnTypes
	{
		Wave
	}
	// The different Enemy levels
	public enum EnemyLevels
	{
		Medium
	}
	//---------------------------------
	// End of the Enums
	//---------------------------------
	
	// Enemy level to be spawnedEnemy
	public EnemyLevels enemyLevel = EnemyLevels.Medium;
	
	//----------------------------------
	// Enemy Prefabs
	//----------------------------------
	public GameObject MediumEnemy;
	private Dictionary<EnemyLevels, GameObject> Enemies = new Dictionary<EnemyLevels, GameObject>(1);
	//----------------------------------
	// End of Enemy Prefabs
	//----------------------------------
	
	//----------------------------------
	// Enemies and how many have been created and how many are to be created
	//----------------------------------
	private int totalEnemy;
	private int waveNum = 0;
	private int numEnemy = 0;
	private int spawnedEnemy = 0;
	//----------------------------------
	// End of Enemy Settings
	//----------------------------------
	
	
	// The ID of the spawner
	private int SpawnID;
	
	//----------------------------------
	// Different Spawn states and ways of doing them
	//----------------------------------
	private bool waveSpawn = false;
	public bool Spawn = true;
	public SpawnTypes spawnType = SpawnTypes.Wave;
	// timed wave controls
	public float waveTimer = 30.0f;
	private float timeTillWave = 0.0f;
	//Wave controls
	public int totalWaves = 5;
	private int numWaves = 0;
	//----------------------------------
	// End of Different Spawn states and ways of doing them
	//----------------------------------
	
	void Start()
	{
		playerOne = GameObject.FindWithTag("Player");
		uiManager = GameObject.FindWithTag("uiManager");
		// sets a random number for the id of the spawner
		SpawnID = Random.Range(1, 500);
		Enemies.Add(EnemyLevels.Medium, MediumEnemy);

		winMenu = GameObject.FindWithTag ("MainCamera");
	}
	// Draws a cube to show where the spawn point is... Useful if you don't have a object that show the spawn point
	void OnDrawGizmos()
	{
		// Sets the color to red
		Gizmos.color = gizmoColor;
		//draws a small cube at the location of the game object that the script is attached to
		Gizmos.DrawCube(transform.position, new Vector3 (0.5f,0.5f,0.5f));
	}
	void Update ()
	{
		if(Spawn)
		{
			if(numWaves < totalWaves + 1)
			{
				if (waveSpawn){
					//spawns an enemy
					spawnEnemy();
				}
				
				if(waveNum <= 5){
					totalEnemy = waveNum;
				}
				else if(waveNum > 5){
					
					//Vertical Slice Win Condition
					disableSpawner (SpawnID);
					winMenu.GetComponent<WinMenu> ().hasWon = true;

					
				}

				if(numEnemy == totalEnemy){
					// disables the wave spawner
					waveSpawn = false;
				}

				if (numEnemy == 0)
				{
					//enables Attr. Screen
					uiManager.GetComponent<UIManagerScript> ().attrScreen.enabled = true;
				}
			}
		}
	}
	// spawns an enemy based on the enemy level randomly that you selected
	private void spawnEnemy()
	{

	GameObject Enemy = (GameObject) Instantiate(Enemies[enemyLevel], new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)), Quaternion.identity);
		Enemy.SendMessage("setName", SpawnID);
		// Increase the total number of enemies spawned and the number of spawned enemies
		numEnemy++;
		spawnedEnemy++;
	}
	// Call this function from the enemy when it "dies" to remove an enemy count
	public void killEnemy(int sID)
	{
		// if the enemy's spawnId is equal to this spawnersID then remove an enemy count
		if (SpawnID == sID)
		{
			numEnemy--;
		}
	}
	//enable the spawner based on spawnerID
	public void enableSpawner(int sID)
	{
		if (SpawnID == sID)
		{
			Spawn = true;
		}
	}
	//disable the spawner based on spawnerID
	public void disableSpawner(int sID)
	{
		if(SpawnID == sID)
		{
			Spawn = false;
		}
	}
	// returns the Time Till the Next Wave, for an interface, ect.
	public float TimeTillWave
	{
		get
		{
			return timeTillWave;
		}
	}
	// Enable the spawner, useful for trigger events because you don't know the spawner's ID.
	public void enableTrigger()
	{
		Spawn = true;
	}

	public void startWave(){
		// enables the wave spawner
		waveSpawn = true;
		//increase the number of waves
		waveNum++;

		/*AudioListener.volume = 1;
		bgm.Play();

		if (bgm.isPlaying == true) {
			AudioListener.volume = 1;
		}*/
	}




}
