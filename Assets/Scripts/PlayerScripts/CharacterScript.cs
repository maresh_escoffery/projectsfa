﻿using UnityEngine;
using System.Collections;

public class CharacterScript : MonoBehaviour {


	public float movementSpeed;

		// ALL FIXED VALUES!!
	public int startingHealth; // doesn't change
	public int startingDamage; // Starting damage
	public int startingPool; //starting pool

		#region attributes
		public int atr_atk, atr_def, atr_hp, pool; // attribute points in whole numbers
		public float atk, def, hp; // actual amounts that change based off of AP
		protected int AP; // Attr. Points
		
		
		#endregion

			
	public CharacterScript(){
			movementSpeed = 1f;

			startingHealth = 10;
			startingDamage = 2;
			startingPool = 3;

			atk = 0;
			def = 0;
			hp = startingHealth;
			pool = startingPool;
			
			atr_atk = 0;
			atr_def = 0;
			atr_hp = 0;
			

		}
}
