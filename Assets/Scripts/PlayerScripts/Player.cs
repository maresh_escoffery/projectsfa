﻿using UnityEngine;
using System.Collections;


//Make movementSpeed and all of it's children a float w/(f)

public class Player: CharacterScript {


	#region constructors
	public Player(){
		movementSpeed = 1f;
		
		startingHealth = 10;
		startingDamage = 2;
		startingPool = 3;

		atk = startingDamage;
		def = 0;
		hp = startingHealth;
		pool = startingPool;
		
		atr_atk = 0;
		atr_def = 0;
		atr_hp = 0;
	}

	/*public Player(float pMovementSpeed, int pStartingHealth, int pStartingDamage, int pStartingPool,
	              int pAtk, int pDef, int pHp, int pPool, int pAtr_atk, int pAtr_def, int pAtr_hp){
		this.movementSpeed = pMovementSpeed;
		this.startingHealth = pStartingHealth;
		this.atk = pAtk;
		this.def = pDef;
		this.hp = pHp;
		this.pool = pPool;
		this.startingDamage = pStartingDamage;
		this.atr_atk = pAtr_atk;
		this.atr_def = pAtr_def;
		this.atr_hp = pAtr_hp;
	}*/
	
	#endregion
	
	public float turnSmoothing = 3.0f;
	public float aimTurnSmoothing = 15.0f;
	public float speedDampTime = 0.1f;


	private Vector3 lastDirection;
	
	private Animator anim;
	private int speedFloat;
	private int hFloat;
	private int vFloat;
	private Transform cameraTransform;
	
	private float h;
	private float v;
	private float distToGround;

	
	private bool isMoving;
	private int groundedBool;
	private int health;

	bool IsGrounded(){
		return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
	}


	void onAttack(){
		if(Input.GetMouseButton(0)){
			Debug.Log("I've attacked.");
		}
	}

	void onBlock(){
		if(Input.GetMouseButton(1)){
			Debug.Log("I've blocked.");
		}
	}


	void Awake()
	{
		anim = GetComponent<Animator> ();
		cameraTransform = Camera.main.transform;
		
		speedFloat = Animator.StringToHash("Speed");
		hFloat = Animator.StringToHash("H");
		vFloat = Animator.StringToHash("V");
		groundedBool = Animator.StringToHash("Grounded");
		distToGround = GetComponent<Collider>().bounds.extents.y;
	}

	
	void Update()
	{
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		isMoving = Mathf.Abs(h) > 0.1 || Mathf.Abs(v) > 0.1;
	}
	
	
	
	void FixedUpdate()
	{
		
		
		anim.SetFloat(hFloat, h);
		anim.SetFloat(vFloat, v);
		
		MovementManagement (h, v);

		anim.SetBool (groundedBool, IsGrounded ());
	}



	#region PlayerMovement
	void MovementManagement(float horizontal, float vertical )
	{
		Rotating(horizontal, vertical);
		
		if(isMoving)
		{
			anim.SetFloat(speedFloat, movementSpeed, speedDampTime, Time.deltaTime);
		}
		else
		{
			anim.SetFloat(speedFloat, 0f);
		}
		GetComponent<Rigidbody>().AddForce(Vector3.forward*movementSpeed);
	}

	Vector3 Rotating(float horizontal, float vertical)
	{
		Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
		
		forward = forward.normalized;
		
		Vector3 right = new Vector3(forward.z, 0, -forward.x);
		
		Vector3 targetDirection;
		
		float finalTurnSmoothing;

		targetDirection = forward * vertical + right * horizontal;
		finalTurnSmoothing = turnSmoothing;

		if((isMoving && targetDirection != Vector3.zero))
		{
			Quaternion targetRotation = Quaternion.LookRotation (targetDirection, Vector3.up);
			
			
			Quaternion newRotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, targetRotation, finalTurnSmoothing * Time.deltaTime);
			GetComponent<Rigidbody>().MoveRotation (newRotation);
			lastDirection = targetDirection;
		}
		if(!(Mathf.Abs(h) > 0.9 || Mathf.Abs(v) > 0.9))
		{
			Repositioning();
		}
		
		return targetDirection;
	}

	private void Repositioning()
	{
		Vector3 repositioning = lastDirection;
		if(repositioning != Vector3.zero)
		{
			repositioning.y = 0;
			Quaternion targetRotation = Quaternion.LookRotation (repositioning, Vector3.up);
			Quaternion newRotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, targetRotation, turnSmoothing * Time.deltaTime);
			GetComponent<Rigidbody>().MoveRotation (newRotation);
		}
	}

	#endregion
	

	/// <summary>
	///  
	/// Temporary code below
	/// 
	/// </summary>




	
}
